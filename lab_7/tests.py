from django.test import TestCase, Client
from django.urls import resolve
from django.http import JsonResponse
from datetime import date
from .views import index, get_mahasiswa_detail, friend_list, get_mahasiswa_list
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper

class Lab7ViewsUnitTest(TestCase):
	# mahasiswa_list
	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)

	def test_lab_7_template_using_lab_7_html(self):
		response = Client().get('/lab-7/')
		self.assertTemplateUsed(response, "lab_7/lab_7.html")

	def test_get_mahasiswa_list_func(self):
		url = "/lab-7/get-mahasiswa-list/"
		found = resolve(url)
		self.assertEqual(found.func, get_mahasiswa_list)

	def test_get_mahasiswa_list(self):
		url = "/lab-7/get-mahasiswa-list/?page=5"
		response = Client().get(url)
		self.assertTrue(isinstance(response, JsonResponse))

		url = "/lab-7/get-mahasiswa-list/?page=-1"
		response = Client().get(url)
		self.assertRedirects(response, "/lab-7/", 302, 200)

	# friend_list
	def test_lab_7_friend_list_is_exist(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_friend_list_using_index_func(self):
		found = resolve('/lab-7/friend-list/')
		self.assertEqual(found.func, friend_list)

	def test_lab_7_friend_list_template_using_lab_7_html(self):
		response = Client().get('/lab-7/friend-list/')
		self.assertTemplateUsed(response, "lab_7/daftar_teman.html")

	# mahasiswa_detail
	def test_lab_7_mahasiswa_detail_url_is_exist(self):
		response = Client().get('/lab-7/mahasiswa/0606104214/')
		self.assertEqual(response.status_code, 200)

	def test_lab_7_mahasiswa_detail_using_index_func(self):
		found = resolve('/lab-7/mahasiswa/0606104214/')
		self.assertEqual(found.func, get_mahasiswa_detail)

	def test_lab_7_mahasiswa_detail_template_using_lab_7_html(self):
		response = Client().get('/lab-7/mahasiswa/0606104214/')
		self.assertTemplateUsed(response, "lab_7/detail_mahasiswa.html")

class Lab7ModelsUnitTest(TestCase):
	def test_create_new_friend(self):
		friend = Friend(friend_name="Ahmad Hasan Siregar", npm="1606892655")
		friend.save()

		self.assertEqual(Friend.objects.all().count(), 1)

	def test_convert_friend_to_json_string(self):
		friend = Friend(friend_name="Ahmad Hasan Siregar", npm="1606892655")
		friend.save()

		expected_string = '{"friend_name": "Ahmad Hasan Siregar", "npm": "1606892655", "added_at": "' + str(date.today()) +'"}'
		json_string = friend.as_json_string()
		self.assertEqual(json_string, expected_string)


class Lab7ApiCSUIHelper(TestCase):
	def test_invalid_sso_username_password(self):
		# create w/ default values -> expected: success
		csui_helper = CSUIhelper(force_create=True)

		# create w/ invalid values -> expected fail
		try:
			csui_helper = CSUIhelper("invalid_username", "invalid_password", True)
		except Exception:
			return

