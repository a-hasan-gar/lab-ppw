from django.conf.urls import url
from .views import index, message_post, message_table

urlpatterns = [
	url(r'^add_message', message_post, name='message_post'),
	url(r'^result_table', message_table, name='result_table'),
	url(r'^$', index, name='index'),
]