from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

author = "Ahmad Hasan Siregar"
response = {"author": author}
csui_helper = CSUIhelper()

# Page halaman menampilkan list mahasiswa yang ada
def index(request):
	mahasiswa_list = csui_helper.instance.get_mahasiswa_list(1)

	friend_list = Friend.objects.all()
	response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list, "author" : author}
	html = 'lab_7/lab_7.html'
	return render(request, html, response)

def get_mahasiswa_list(request):
	if request.method == 'GET':
		pageNumber = int(request.GET.get("page", 1))
		if 1 <= pageNumber <= 68:
			mahasiswa_list = csui_helper.instance.get_mahasiswa_list(pageNumber)
			data = {'mahasiswa_list': mahasiswa_list}
			return JsonResponse(data)
		else:
			return HttpResponseRedirect('/lab-7/')

def get_mahasiswa_detail(request, npm):
	if request.method == 'GET':
		mahasiswa = csui_helper.instance.get_mahasiswa_detail(npm)
		response["mahasiswa"] = mahasiswa
		html = "lab_7/detail_mahasiswa.html"

		return render(request, html, response)

def friend_list(request):
	friend_list = Friend.objects.all()
	response['friend_list'] = friend_list
	html = 'lab_7/daftar_teman.html'
	return render(request, html, response)

def friend_list_json(request):
	data = {
		'friend_list': models_to_json_array(Friend.objects.all())
	}
	return JsonResponse(data)

@csrf_exempt
def add_friend(request):
	if request.method == 'POST':
		name = request.POST['name']
		npm = request.POST['npm']
		# friend already exist
		if (Friend.objects.filter(npm=npm).exists()):
			return HttpResponse("Mahasiswa sudah ditambah menjadi teman", status=400)
		# friend not yet in db
		else:
			friend = Friend(friend_name=name, npm=npm)
			friend.save()
			data = friend.as_json_string()
			return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
	if request.method == 'POST':
		Friend.objects.filter(npm=request.POST["npm"]).delete()
		data = {"deleted_npm" : request.POST["npm"]}
		return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
	npm = request.POST.get('npm', None)
	data = {
		'is_taken': Friend.objects.filter(npm=npm).exists()
	}
	return JsonResponse(data)

def models_to_json_array(obj):
	data = []
	for mhs in obj:
		data.append(json.loads(mhs.as_json_string()))
	return data
