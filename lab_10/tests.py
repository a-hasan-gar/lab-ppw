import os, environ
from django.test import TestCase
from django.urls import reverse, resolve
from django.http.cookie import SimpleCookie
from .views import *

# Create your tests here.
class Lab10UnitTest(TestCase):
	def setUp(self):
		root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
		env = environ.Env(DEBUG=(bool, False))
		environ.Env.read_env('.env')
		self.sso_username = env("SSO_USERNAME")
		self.sso_password = env("SSO_PASSWORD")

	# custom_auth
	def test_custom_auth_login(self):
		data = {'username' : self.sso_username, 'password' : self.sso_password}
		response = self.client.post('/lab-10/custom_auth/login/', data)
		self.assertRedirects(response, reverse('lab-10:index'), 302, 302)

		session = self.client.session
		self.assertEqual(session['user_login'], data['username'])
		self.assertTrue('access_token' in session)
		self.assertTrue('kode_identitas' in session)
		self.assertTrue('role' in session)

	def test_custom_auth_login_invalid(self):
		data = {'username' : 'user', 'password' : 'pass'}
		response = self.client.post('/lab-10/custom_auth/login/', data)
		self.assertRedirects(response, reverse('lab-10:index'), 302, 200)
		session = self.client.session
		self.assertTrue('user_login' not in session)
		self.assertTrue('access_token' not in session)
		self.assertTrue('kode_identitas' not in session)
		self.assertTrue('role' not in session)

	def test_custom_auth_logout(self):
		data = {'username' : self.sso_username, 'password' : self.sso_password}
		self.client.post('/lab-10/custom_auth/login/', data)

		response = self.client.get('/lab-10/custom_auth/logout/')
		self.assertRedirects(response, reverse('lab-10:index'), 302)
		session = self.client.session
		self.assertTrue('user_login' not in session)
		self.assertTrue('access_token' not in session)
		self.assertTrue('kode_identitas' not in session)
		self.assertTrue('role' not in session)