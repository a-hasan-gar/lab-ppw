# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
#catatan: tidak bisa menampilkan messages jika bukan menggunakan method 'render'
from .api_enterkomputer import get_drones, get_soundcards, get_opticals

response = {}

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

# ======================================================================== #
# Menerima sebuah request dan me-return object render html untuk 2 kondisi:
# profile jika request sudah memiliki session (user sudah login), selain itu
# return html login
def index(request):
	print ("#==> masuk index")
	if 'user_login' in request.session:
		return HttpResponseRedirect(reverse('lab-9:profile'))
	else:
		html = 'lab_9/session/login.html'
		return render(request, html, response)

# ======================================================================== #
# Dipanggil oleh function profile untuk memasukkan data-data user
# yang didapat dr akun SSO (lihat: csui_helper.py) ke dalam object response
def set_data_for_session(res, request):
	response['author'] = request.session['user_login']
	response['access_token'] = request.session['access_token']
	response['kode_identitas'] = request.session['kode_identitas']
	response['role'] = request.session['role']
	response['drones'] = get_drones().json()
	response['soundcards'] = get_soundcards().json()
	response['opticals'] = get_opticals().json()

	# print ("#drones = ", get_drones().json(), " - response = ", response['drones'])
	response['fav_drones'] = request.session.get('drones', [])
	response['fav_soundcards'] = request.session.get('soundcards', [])
	response['fav_opticals'] = request.session.get('opticals', [])

# ======================================================================== #
# Meng-handle request profile user
def profile(request):
	print ("#==> profile")
	## sol : bagaimana cara mencegah error, jika url profile langsung diakses
	if 'user_login' not in request.session.keys():
		return HttpResponseRedirect(reverse('lab-9:index'))
	## end of sol

	set_data_for_session(response, request)

	html = 'lab_9/session/profile.html'
	return render(request, html, response)

# ======================================================================== #

### General Function
def add_session_item(request, key, id):
	print ("#ADD session item")
	ssn_key = request.session.keys()
	if not key in ssn_key:
		request.session[key] = [id]
	else:
		items = request.session[key]
		if id not in items:
			items.append(id)
			request.session[key] = items

	msg = "Berhasil tambah " + key + " favorite"
	messages.success(request, msg)
	return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_item(request, key, id): 
	print ("# DEL session item")
	items = request.session[key]
	print ("before = ", items)
	items.remove(id)
	request.session[key] = items
	print ("after = ", items)

	msg = "Berhasil hapus item " + key + " dari favorite"
	messages.error(request, msg)
	return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_item(request, key):
	if key in request.session.keys():
		del request.session[key]
		msg = "Berhasil hapus session : favorite " + key
		messages.error(request, msg)
		
	return HttpResponseRedirect(reverse('lab-9:index'))

# ======================================================================== #
# COOKIES

# Menampilkan halaman profile (cookie) jika user sudah login, sebaliknya
# menampilkan halaman login (cookie)  jika user belum login
def cookie_login(request):
	print ("#==> masuk login")
	if is_login(request):
		return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
	else:
		html = 'lab_9/cookie/login.html'
		return render(request, html, response)

# Meng-handle request authentication dengan Cookies
def cookie_auth_login(request):
	print ("# Auth login")
	if request.method == "POST":
		user_login = request.POST['username']
		user_password = request.POST['password']

		if my_cookie_auth(user_login, user_password):
			print ("#SET cookies")
			res = HttpResponseRedirect(reverse('lab-9:cookie_login'))

			res.set_cookie('user_login', user_login)
			res.set_cookie('user_password', user_password)

			return res
		else:
			msg = "Username atau Password Salah"
			messages.error(request, msg)
			return HttpResponseRedirect(reverse('lab-9:cookie_login'))
	else:
		return HttpResponseRedirect(reverse('lab-9:cookie_login'))

def cookie_profile(request):
	print ("# cookie profile ")
	# method ini untuk mencegah error ketika akses URL secara langsung
	if not is_login(request):
		print ("belum login")
		return HttpResponseRedirect(reverse('lab-9:cookie_login'))
	else:
		# print ("cookies => ", request.COOKIES)
		in_uname = request.COOKIES['user_login']
		in_pwd= request.COOKIES['user_password']

		if my_cookie_auth(in_uname, in_pwd):
			html = "lab_9/cookie/profile.html"
			res =  render(request, html, response)
			return res
		else:
			print ("#login dulu")
			msg = "Kamu tidak punya akses :P "
			messages.error(request, msg)
			html = "lab_9/cookie/login.html"
			return render(request, html, response)

def cookie_clear(request):
	res = HttpResponseRedirect(reverse("lab-9:cookie_login"))
	res.delete_cookie('user_login')
	res.delete_cookie('user_password')

	msg = "Anda berhasil logout. Cookies direset"
	messages.info(request, msg)
	return res

# Mencocokkan data dari form login (lihat: cookie_login) dengan suatu constant yang dipilih
def my_cookie_auth(in_uname, in_pwd):
	my_uname = "Azurey"
	my_pwd = "pass"
	return in_uname == my_uname and in_pwd == my_pwd

# Mengecek apakah suatu request sudah memiliki data login
# i.e. keys user_login dan user_password
def is_login(request):
	return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES
