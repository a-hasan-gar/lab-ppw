// Chatbox
$(document).ready(function(){
	var counter = 0;
	$("#chat-content").keypress(function(event) {
		if (event.keyCode == 13 || event.which == 13) {
			// get and clear current chat
			var chatContent = $("#chat-content").val();
			$("#chat-content").val('');

			// insert new chat to html
			var chatClass = (counter == 0 ? 'msg-send' : 'msg-receive');
			counter = (counter ? 0 : 1);
			var newChat = $('<div class="' + chatClass + '">' + chatContent + '</div>')
			$(".msg-insert").append(newChat);
		}
	});
});
// END

// Calculator
var print = document.getElementById('print');

var go = function(x) {
	if (x === 'ac') {
		print.value = "";
	} else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
	} else if (x === 'log' || x === 'sin' || x === 'tan') {
		print.value = Math.round(evil('Math.' + x + '(' + print.value + ')') * 10000) / 10000;

	} else {
		print.value += x;
	}
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

// Theme switching
// populate options
$(document).ready(function() {
	if (Storage) {
		localStorage.themes = `[
			{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#0036f6"},
			{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#0036f6"},
			{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#0036f6"},
			{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#0036f6"},
			{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
			{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
			{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
			{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
			{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
			{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
			{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#0036f6"}
		]`;
		localStorage.selectedTheme = JSON.stringify({"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#0036f6"}); 
		// set default theme
		changeBodyTheme(JSON.parse(localStorage.selectedTheme));

		$('.my-select').select2({
			'data' : JSON.parse(localStorage.themes)
		});
	}
	else {
		console.log("Sorry, theme switching isn't compatible with your browser (doesn't support web storage).")
	}
});

// set event listener
$('.apply-button').on('click', function(){ 
    var selectedThemeId = $(".my-select").val();
    var selectedTheme = JSON.parse(localStorage.themes)[selectedThemeId];
    changeBodyTheme(selectedTheme);
    
    localStorage.selectedTheme = JSON.stringify(selectedTheme);
})

function changeBodyTheme(selectedTheme) {
	$('body').css({
    	"background" : selectedTheme.bcgColor,
    	"color" : selectedTheme.fontColor
    });
}
// END