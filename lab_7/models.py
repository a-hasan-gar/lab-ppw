from django.db import models
import json

class Friend(models.Model):
	friend_name = models.CharField(max_length=400)
	npm = models.CharField(max_length=250, primary_key=True)
	added_at = models.DateField(auto_now_add=True)

	def as_json_string(self):
		data = {"friend_name" : self.friend_name, "npm" : self.npm, "added_at": str(self.added_at)}
		return json.dumps(data)
